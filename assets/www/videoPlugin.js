/**
 * Constructor
 */
function VideoPlugin() {
};

/**
 * Starts the video player intent
 *
 * @param url           The url to play
 */
VideoPlugin.prototype.play = function(swypeEventCallback, failureCallback, url) {
    cordova.exec(swypeEventCallback, failureCallback, "VideoPlugin", "playVideo", [url]);
};

/**
 * Load VideoPlayer
 */
if(!window.plugins) {
    window.plugins = {};
}
if (!window.plugins.VideoPlugin) {
    window.plugins.VideoPlugin= new VideoPlugin();
}