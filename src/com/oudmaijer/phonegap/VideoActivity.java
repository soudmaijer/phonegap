package com.oudmaijer.phonegap;

import android.app.Activity;
import android.content.Intent;
import android.gesture.Gesture;
import android.gesture.GestureOverlayView;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.VideoView;

public class VideoActivity extends Activity {

    private final String TAG = "VideoActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        String videoUrl = getIntent().getStringExtra("videoUrl");
        Log.d(TAG, "Displaying videoUrl: " + videoUrl);
        Uri uri = Uri.parse(videoUrl);

        VideoView video = (VideoView) findViewById(R.id.videoView);
        video.setVideoURI(uri);
        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        GestureOverlayView gestures = (GestureOverlayView) findViewById(R.id.gestures);
        gestures.addOnGesturePerformedListener(handleGestureListener);

        // Start the video
        video.start();
    }

    /**
     * our gesture listener
     */
    private GestureOverlayView.OnGesturePerformedListener handleGestureListener = new GestureOverlayView.OnGesturePerformedListener() {
        @Override
        public void onGesturePerformed(GestureOverlayView gestureView, Gesture gesture) {
            Log.d(TAG, "Gesture detected");
            Intent intent = new Intent();
            setResult(1, intent);
            finish();
        }
    };

}
