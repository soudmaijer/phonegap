package com.oudmaijer.phonegap;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import org.apache.cordova.api.Plugin;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class VideoPlugin extends Plugin {

    public static final String TAG = "VideoPlugin";
    public static final String PLAY_VIDEO_ACTION = "playVideo";
    public static int SWYPE_EVENT_REQUEST = 100;
    public String callbackId;

    @Override
    public PluginResult execute(String action, JSONArray data, String callbackId) {
        this.callbackId = callbackId;
        String result = "";
        PluginResult.Status status = PluginResult.Status.OK;

        Log.d(TAG, "execute() called. Action: " + action);

        try {
            if (PLAY_VIDEO_ACTION.equals(action)) {
               playVideo(data.getString(0));
            } else {
                status = PluginResult.Status.INVALID_ACTION;
            }
            return new PluginResult(status, result);
        } catch (JSONException e) {
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
    }

    private void playVideo(String videoUrl) {
        Intent intent = new Intent(this.cordova.getActivity(), VideoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("videoUrl", videoUrl);
        intent.putExtras(bundle);
        
        Log.d(TAG, "Launching startActivity: " + videoUrl);
        this.cordova.startActivityForResult((Plugin) this, intent, SWYPE_EVENT_REQUEST);
    }

    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == SWYPE_EVENT_REQUEST){
			Bundle b = data.getExtras();
            Log.d(TAG, "Got Swype event from VideoView");
            handleSwipeEventCallback(b);
        }
    }

    private void handleSwipeEventCallback(Bundle data) {
        Log.d(TAG, "handleSwipeEventCallback() called.");
        PluginResult result;
        try {
            JSONObject resultJSON = new JSONObject();
            resultJSON.put("swypeEvent", true);
            result = new PluginResult(PluginResult.Status.OK, resultJSON);
        } catch (JSONException jsonEx) {
            Log.e(TAG, "Got JSON Exception " + jsonEx.getMessage());
            jsonEx.printStackTrace();
            result = new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
        //callback to the javascript layer with our result
        //using the held callbackId via success(PluginResult result, String callbackId)
        result.setKeepCallback(false);
        this.success(result, this.callbackId);
    }
}